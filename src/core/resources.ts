export interface Link {
    __typename: "Link";
    title: string;
    adress: string;
    is_done: boolean; 
}

export interface Text {
    __typename: "Text";
    content: string;
    is_done: boolean;
}

export interface Null {
    __typename: "Null";
}

export type Resource = Link | Text | Null;
export type ResourceTypeString = Resource["__typename"]

export const ResourceTypeStringValues: Array<ResourceTypeString> = [
    "Link",
    "Text",
    "Null"
]

export default {};