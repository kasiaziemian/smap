"use strict";
exports.__esModule = true;
var express_1 = require("express");
var body_parser_1 = require("body-parser");
var app = express_1["default"]();
app.use(body_parser_1["default"].json());
app.use(body_parser_1["default"].urlencoded({ extended: true }));
app.get("/", function (req, res) {
    res.send("run");
});
var PORT = process.env.PORT;
app.listen(PORT, function () {
    console.log("server run on port " + PORT);
});
