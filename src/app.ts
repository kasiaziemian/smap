import express, {Application, Request, Response, NextFunction} from "express"
import bodyParser from "body-parser"

const app: Application = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get("/", (req: Request, res: Response) => {
    res.send("run")
})


app.listen(3000, () => {
    console.log(`server run on port 3000`)
})